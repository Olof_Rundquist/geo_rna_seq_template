#!/bin/bash -l

#SBATCH -J STAR
#SBATCH -n 32
#SBATCH -t 02:00:00

samples=${1}
genomedir=/proj/lassim/reference_genomes_OR/human/hg38_refseq/star_index

STAR --runThreadN 16 --outSAMtype BAM SortedByCoordinate --outSAMstrandField intronMotif --outFilterIntronMotifs RemoveNoncanonical --readFilesCommand zcat --genomeDir $genomedir --readFilesIn ${samples} --outTmpDir ${samples%%.*}.tmp. --outFileNamePrefix ${samples%%.*}.star.
