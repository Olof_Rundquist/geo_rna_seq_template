#!/bin/bash -l

#Find all of samples
samples=($(find ./fastq/SRR*fastq.gz -type f | sort -z))

mkdir ./trimmed_galore # make the output directory

for index in ${!samples[*]} # This is taken from here: https://stackoverflow.com/questions/17403498/iterate-over-two-arrays-simultaneously-in-bash.
do
    sbatch ./trim_galore_SE.sh "${samples[index]}" 
done
