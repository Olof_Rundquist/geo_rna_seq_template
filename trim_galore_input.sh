#!/bin/bash -l

#Find all of samples. Divde into forward and reverse. The sort is there to make sure they are in the same order. 
forward=($(find ./fastq/SRR*_1*fastq.gz -type f | sort -z))
reverse=($(find ./fastq/SRR*_2*fastq.gz -type f | sort -z)) 
#Note: The parantheses around the $(find etc) converts the find object into a list. 
#This is important beacuse th find object itself does not have an index. 

#Give the files for each sample to trim galore. Each sample usually have multiple forward and reverse read files.
#Trim galore expects input on the form of forward reverse forward reverse and so on.

mkdir ./trimmed_galore # make the output directory

for index in ${!forward[*]} # This is taken from here: https://stackoverflow.com/questions/17403498/iterate-over-two-arrays-simultaneously-in-bash.
do
    sbatch ./trim_galore.sh "${forward[index]} ${reverse[index]}" # ${forward[index]##./*/} # ${forward[index]##./*/} = file name 
done
