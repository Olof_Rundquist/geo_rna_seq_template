#!/usr/bin/env python

import pandas as pd
import sys
import subprocess
import copy

#anno=pd.read_csv("../SRA_GSE70050.csv")
anno=pd.read_csv(sys.argv[2]) # The metadata file

d={}
for i in range(len(anno)): 
    d[pd.Series(anno.SampleName)[i]]=[]
    d[pd.Series(anno.SampleName)[i]].append(pd.Series(anno.Run)[i])
    

#Adds the file extension
if sys.argv[1] == "SE" : # If single end data
    print("Single end mode")
    for i in d.keys(): 
        for x in range(len(d[i])):
            d[i][x] = "./trimmed_galore/" + d[i][x] + "_pass_trimmed.fq.gz" # Add the file extension of the desired input files. Change if necessary. Note that d[i][x] = SRR##### so any part of the file name that is not that has to be added.
    for i in d.values(): #Call the star aligner using the subprocees.call function.
        x = ",".join(i)
        y = "sbatch star_SE.sh  " + x
        subprocess.call(y, shell=True) # gives the files as a comma delimited list to the star script.

elif sys.argv[1] == "PE" : # If paired end data
    print("Paired end mode")
    forward = copy.deepcopy(d)
    reverse = copy.deepcopy(d)
    
    for i in forward.keys():
        for x in range(len(forward[i])):
            forward[i][x] = "./trim_galore/" + forward[i][x] + "_pass_1_trimmed.fq.gz" # Add the file extension of the desired input files. Change if necessary.
    for i in reverse.keys():
        for x in range(len(reverse[i])):
            reverse[i][x] = "./trim_galore/" + reverse[i][x] + "_pass_2_trimmed.fq.gz" # Add the file extension of the desired input files. Change if necessary.

    for m,n in zip(forward.values(), reverse.values()): #Call the star aligner using the subprocees.call function.
        x = ",".join(m)
        z = ",".join(n)
        y = "sbatch star_PE.sh  " + x + " " + z
        #print y
        subprocess.call(y, shell=True) # gives the files as a comma delimited list to the star script.
