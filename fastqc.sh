#!/bin/bash -l

#SBATCH -J fastqc
#SBATCH -n 32
#SBATCH -t 02:00:00

mkdir ./fastq/fastqc
n=$(find ./fastq/SRR*_pass*.fastq.gz | wc) # counts the number of input files. 
#Note that fastqc uses about 250 mb of ram per thread so each node with 96gb ram across 32 cores can support 384 threads. If you try more fastqc will not start. 
fastqc -t ${n} -o ./fastq/fastqc ./fastq/SRR*_pass*.fastq.gz

cd ./fastq/fastqc

multiqc *zip

