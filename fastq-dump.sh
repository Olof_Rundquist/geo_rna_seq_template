#!/bin/bash -l

#SBATCH -J SRA-extract
#SBATCH -n 1 #Number of cores to use. In this case 1. 
#SBATCH -t 02:00:00

#This is not a very demanding job and is not improved by giving more cores.

# $1 is the sra file.
fastq-dump --skip-technical --readids --read-filter pass --dumpbase --split-3 --clip --gzip -O ./fastq $1
