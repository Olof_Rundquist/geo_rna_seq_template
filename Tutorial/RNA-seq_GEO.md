## RNA-seq analysis guide on sigma. From GEO to fastq to alignment.
Hello! Welcome to my tutorial for RNA-seq anlysis of GEO datasets on the sigma cluster. This tutorial will walk you through at a very basic level how to setup RNA-seq analysis on sigma. 
The tutorial is written to be understandable to people without prior knowledge of programing or unix based operating systems.
Therefore a crash course in both is given as well. The principles outlined in this tutorial also applies to other nges analysis pipelines. 
The tutorial is written from a meta analysis perspective where raw data is downloaded and a standardized RNA-seq pipeline is applied to it. Good luck.

### Getting started 
Start by acquiring access to the sigma cluster by following the instructions on the snic webpage.
Connect to the cluster through ssh. On a Linux system this should be directly available in the terminal window. 
On windows I recommend you to download mobaxterm and/or thinlinc and log in through its terminal. Download the portable version of mobaxterm since it bypasses the need for Administrator rights.
For thinlinc: Check the intructions for setting up thinlinc on the nsc webpage. Thinlinc is a lot better than mobaxterm for displaying graphical applications and less vulnerable to bad internet. 
However it is impractical for moving data between the cluster and your local computer. I recommend to use both, mobaxterm for data moving and thinlinc for scripting and job submissions. Tip: To get out of full screen mode in thinlinc use F8.

Some useful bash commands:
	
	For help do man command or --help (ex: `man cd` or `cd --help`)
	
    1. cd = change directory (ex: `cd $HOME` = change directory to home directory.)
	
    2. mkdir = make directory (ex: `mkdir scripts`)
	
    3. cp = copy file
	
    4. mv = move file (think ctr x) note that mv and cp will overwrite files without asking if they have the same name.
	
    5. rm = remove file, Note that removed files are not recoverable. If you are unsure about what you are deleting use -i and rm will ask you before it deletes it. You can use rm to delete your whole home directory so be careful with it.
	
    6. pwd = print path to current directory.
	
    7. scp = secure copy (ex: to copy files from your computer to home on sigma: scp ./file.txt user@sigma.nsc.liu.se:/home/)
	
    8. echo ${varible} = print variable to screen. ex if i=hello, echo ${i} prints hello to the screen.
	
    For futher tips see this manual: https://www.gnu.org/software/bash/manual/bashref.html

### Setup up local sequencing environment on Sigma. 
Unlike UPPMAX, sigma does not have most of the necessary tools for bioinformatics installed. Therefor you have to install them yourself to your local profile.
The simplest way to do this is to create a virtual environment using conda and install the software into it. 
The alternative is to install everything from source but since most sequencing software has many dependencies this gets very complex very quickly. 
Conda automatically handles dependencies and makes sure that all of your software is compatible with each other. The downside with conda is that it takes up more space.
To use conda load the Anaconda module and the buildenv module. Then create a conda virtual environment in your local folder. 

```bash
module load Python/3.6.3-anaconda-5.0.1-nsc1
module load buildenv-intel/2018a-eb
conda create -n enviroment_name
source activate enviroment_name

#Set up channels to install packages. You can use addtional channels by doing conda install -c channel.
conda config --add channels defaults
conda config --add channels conda-forge
conda config --add channels bioconda
```
Bioconda is conda repository for biological analysis tools. It contains many useful packages for HTseq analysis. Simply google bioconda to check their available packages.

Now install the necessary packages for the tutorial.

```bash
conda install -y sra-tools fastqc multiqc trim-galore samtools picard star stringtie 
module load git
git clone  https://github.com/pepkit/geofetch
wget http://ccb.jhu.edu/software/stringtie/dl/prepDE.py
```
##### What are we installing?

sra-tools: A set of tools used to interact with the SRA database.

geofetch: Handy python script that downloads whole SRA dataset given GSE number. Install as instructed on github. https://github.com/pepkit/geofetch

Fastqc: Sequence Quality check tool

Multiqc: Tool that summarizes multiple fastqc reports into one.

trim-galore: Tool used to remove sequencing adapters from reads and discard low quality reads.

samtools: Tool to manipulate sam and bam files.

picard: Another tool to manipulate sam and bam files.

star: A sequence aligner. Aligns RNA-seq reads to the reference genome.

stringtie: Expression quantifier and transcript assembler.

prepDE.py: Script to summarize results from stringtie. Download from here: http://ccb.jhu.edu/software/stringtie/dl/prepDE.py

To make it so that your anaconda module is always loaded open .bashrc in your home directory and add the line `module load python/3.6.1-anaconda-5.0.1` to it.
While your at it for easy access to your RNA-seq data also add it's location as an environment varibel: `export RNA-seq=<path_to_RNA-seq_directory>` (ex /proj/lassim/users/x_oloru/geo). 
You can now access your RNA-seq data directory by simply typing `cd $RNA-seq`

Also add the following lines to your .bashrc script. This will allow you to call these scripts directly from the command line. Change the paths as necessary.
PATH=$PATH:/home/$USER/geofetch/geofetch
PATH=$PATH:/home/$USER/scripts/prepDE.py
.bashrc is your profile settings script.

### Downloading data from GEO.
The SRA-tools are not the best documented tools and are in general not very cooperative. Luckily geofetch mostly solves this and allows you to download whole datasets without having to download the metadata first and make lists.
To start downloading data from geo go to your project directory (/proj/$project/users/$USER/) and make a directory called geo. (change $project to your project)
cd into geo and clone the gitlab repository into it. `git clone https://gitlab.com/Olof_Rundquist/geo_RNA_seq_template.git`. This gives you access to the scriptes outlined in this tutorial.   
Now use Geofetch to download the metadata and raw sequencing data.
Note that geofetch.py (and prepDE.py) are written in python 2.7 which is obsolete and therefore incompatible with modern python. 
To make sure that geofetch.py uses the right python open geofetch.py with emacs (or vim or nano or use sed) and change the first line (#!/usr/bin/env python) to (#!/usr/bin/python2.7).  
The previous line was telling the script to use the python of your current environment, most likely python 3.6. The new line tells it to use python2.7.

To download a dataset from geo use the provided geofetch_input.sh script. Run as: `geofetch_input.sh GSE######`

##### geofetch_input.sh
```bash
#!/bin/bash -l

#Run this in the directory you want the data. This will create a directory with the sra data named after the dataset.

GSE=${1}

mkdir ${GSE}
mkdir ${GSE}/metadata
mkdir ${GSE}/sra

#Set the download paths. These are used by geofetch.
export SRAMETA=${GSE}/metadata
export SRARAW=${GSE}/sra

#This requires python 2.7
geofetch.py -i $GSE
```

This should give you a directory named as the dataset with the sra data in it. If the data is not there do `echo $SRARAW` and check the returned directory and move it to the project.
If it behaves this way then check out the fix on the geofetch github page. Now copy all the scripts from the template into the geo/GSE##### folder.

### Extract reads from sra files
The files you download from geo are encoded to save space so they need to be extracted. This is done with the fastq-dump program. Read about fastq-dump here: https://edwards.sdsu.edu/research/fastq-dump/
Fastq-dump is an "interesting" program as the default settings are probably only applicable to out of date single end DNA-seq and many of the options of the program are hidden and not in the help information.

Fastq-dump is used on each file one by one and is therefore best to parallelize. This we will do through a for loop that submits multple jobs to the cluster. Jobs on the cluster is submitted through sbatch. 
Read about it here: https://www.nsc.liu.se/support/batch-jobs/sigma/.
Essentially what sbatch does is to submit your script to a queue and runs it on the different nodes of the cluster as they become available.
The scripts:

##### fastq-dump_input.sh
```bash
#!/bin/bash -l

#Find the files and save them to a variable.
samples=$(find ./sra/*sra -type f) 

for i in ${samples}
do
sbatch fastq-dump.sh ${i}
done
```

As you can see this part is quite easy. the $ in front of i calls the varible i. The curly braces ({}) in this case does nothing but it's good practice to have them as they mark what is a varible and what is not.
Ex: i = hello. echo ${i} gives hello, echo ${i}world gives helloworld. This will be useful later.

The script we are giving input to:

##### fastq-dump.sh
```bash
#!/bin/bash -l

#SBATCH -J SRA-extract
#SBATCH -n 1 #Number of cores to use. In this case 1. 
#SBATCH -t 02:00:00

#This is not a very demanding job and is not improved by giving more cores.

mkdir ./fastq

# $1 is the sra file.
fastq-dump --skip-technical --readids --read-filter pass --dumpbase --split-3 --clip --gzip -O ./fastq $1
```
For an explanation of the parameters read the linked webpage I provided. Note this is for paired end data. If your data is single end remove --readids and --split-3 as they will split your single end reads in the middle.
This will produce 2-3 three files for each sra file in the folder fastq. One forward read file (SRR#####_1.fastq.gz) one reverse read file (SRR#####_2.fastq.gz) and one for unpaired reads (SRR#####_3.fastq.gz). The third file is not always present and is not used.


### Pre-Alignment processing 
Next we need to check the quality of the data. One would think that if you submit data to GEO it would be of good ready to use quality but this is not usually the case. To check quality we will use fastqc.  

For Fastqc the for loop like above won't be necessary since the parallelization is built into the program itself. 
Fastqc will check the number of reads, read quality, adapter content etc of your files and output a html report for each file. For each html report it will also output a zip file.
This report is quite useful but if you have many files it is very cumbersome to go through all of the reports. We will therefore compile all of the reports using multiqc. 

##### fastqc.sh
```bash
#!/bin/bash -l

#SBATCH -J fastqc
#SBATCH -n 32
#SBATCH -t 02:00:00

mkdir ./fastq/fastqc
n=$(find ./fastq/SRR*_pass*.fastq.gz | wc) # counts the number of input files. 
#Note that fastqc uses about 250 mb of ram per thread so each node with 96gb ram across 32 cores can support 384 threads. If you try more fastqc will not start. 
fastqc -t ${n} -o ./fastq/fastqc ./fastq/SRR*_pass*.fastq.gz

cd ./fastq/fastqc

multiqc *zip

```
To submit the above script to the job queue write `sbatch fastqc.sh`.
Read the fastqc manual to know what to look for in the multiqc report. Mainly we will care about the sequencing depth (number of reads), sequence quality, read duplication percentage and adapter content.
If your reads are of good quality (score average 30-40) and you have no sequencing adapters left skip the next step and proceed directly to alignment.
However read it anyway since the rest will not make sense otherwise.

#### Trimming adapters and cleaning up bad reads.
If you have files failing the sequencing adapter content check in the multiqc report you need to remove theses adapters.
To achieve this we will be using trim galore. 
Trim galore and similar programs like trimmomatic is used since they preserve the structure of the input file. This is vital if you have paired end data as otherwise you will lose the pairing information.
Once again since we want to parallelize trim galore we will write this in two scripts where the first script gives input to the next.

Below are the scripts: Alternative scripts for single end data are avilbele (marked SE). 

##### trim_galore_input.sh
```bash
#!/bin/bash -l

#Find all of samples. Divde into forward and reverse. The sort is there to make sure they are in the same order.
forward=$(find ./fastq/SRR*_1*fastq.gz -type f | sort -z)
reverse=$(find ./fastq/SRR*_2*fastq.gz -type f | sort -z) 

#Give the files for each sample to trim galore. Each sample usually have multiple forward and reverse read files.
#Trim galore expects input on the form of forward reverse forward reverse and so on.

mkdir ./trimmed_galore # make the output directory

for index in ${!forward[*]} # This is taken from here: https://stackoverflow.com/questions/17403498/iterate-over-two-arrays-simultaneously-in-bash.
do
    sbatch ./bin/trim_galore.sh "${forward[index]} ${reverse[index]}" # ${forward[index]##./*/} # ${forward[index]##./*/} = file name 
done

```
Let's break down the for loop above:
for index in ${!forward[*]} : gets the index of the array: See here for explanation: https://www.linuxjournal.com/content/bash-arrays
do and done is used to encase for loops in bash.
"${forward[index]} ${reverse[index]}" = Note the encassing "". This allows you give both of these as one argument. The {} are also very importatn as they callthe varaible otherwise this would just be the string "$forward[index] etc" and not the actual file names.
${forward[index]##./*/} = this is an example of Parameter expansion. What it does is to match what commes after ## to the beginning of the variable and removes any character that matches. 
If you use %% the end of the string is matched instead. This is useful for removing file extensions. Note: This turned out to be unnecessary and is therefore commented out in the script.


The script we are giving input to:

##### trim_galore.sh
```bash
#!/bin/bash -l

#SBATCH -t 02:00:00
#SBATCH -n 4
#SBATCH -J Trim_galore

files=${1} # ${1} = input 1. The list of files from the above script.

trim_galore -o ./trimmed_galore/ --nextera --fastqc --paired --retain_unpaired ${files} 

```
The trim-galore input:
--nextera = the adpter used for sequencing. Check beforehand.
--fastqc = run fastqc on the output file. This allows you to check that the adapters were removed.
--paired = Paired end mode for Paired end sequencing
--retain_unpaired = keep unpaired reads and write them to a separate file.

### Alignment
STAR and stringtie

First build the star index:
The files are:
GCA_000001405.15_GRCh38_no_alt_analysis_set.fna # The genome. Download with wget .
GCA_000001405.15_GRCh38_full_analysis_set.refseq_annotation.gff # The annotation may need to be converted to gtf as it was below. This can be done with multiple software. I recommend cufflinks.

The index exists in the in: /proj/lassim/reference_genomes_OR/human/hg38_refseq/star_index so skip this step. This directory is open to all group members. There are also indexes available for hg19 as well.
There is both refseq and ensemble versions avilabel.
##### make_star_index.sh
```bash
#!/bin/bash -l

#SBATCH -J STAR_Index
#SBATCH -t 08:00:00
#SBATCH -n 32
#SBATCH -N 1

mkdir ./star_index

STAR --runMode genomeGenerate --runThreadN 16 --genomeDir /proj/lassim/reference_genomes_OR/human/hg38_refseq/star_index --genomeFastaFiles /proj/lassim/reference_genomes_OR/human/hg38_refseq/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna --sjdbGTFfile /proj/lassim/reference_genomes_OR/human/hg38_refseq/GCA_000001405.15_GRCh38_full_analysis_set.refseq_annotation.gtf
```

Align with star. 

For the alignment we will merge all files for each sample into one alignmant. 
Which file belongs to which sample is not apparent from the SRR number so for this we will have to use a rather complicated python script that groups files based on the metadata.
Note that the SRA_GSE######.csv file does not always exist. In these cases you will have to look into the rest of the metadata and see if you can find an equivalent information file. This usually also means that you will have to edit the below script to group by the equivalent columns of the new metadata file.
This is probably doable in bash through grep but that is beyond me.

##### star_input.py
```python

#!/usr/bin/env python

import pandas as pd
import sys
import subprocess
import copy

#anno=pd.read_csv("../SRA_GSE70050.csv")
anno=pd.read_csv(sys.argv[2]) # The metadata file

d={}
for i in range(len(anno)): 
    d[pd.Series(anno.SampleName)[i]]=[]
    d[pd.Series(anno.SampleName)[i]].append(pd.Series(anno.Run)[i]) # If other metadata file change here.
    

#Adds the file extension
if sys.argv[1] == "SE" : # If single end data
    print("Single end mode")
    for i in d.keys(): 
        for x in range(len(d[i])):
            d[i][x] = "./trimmed_galore/" + d[i][x] + "_pass_trimmed.fq.gz" # Add the file extension of the desired input files. Change if necessary. Note that d[i][x] = SRR##### so any part of the file name that is not that has to be added.
    for i in d.values(): #Call the star aligner using the subprocees.call function.
        x = ",".join(i)
        y = "sbatch star_SE.sh  " + x
        subprocess.call(y, shell=True) # gives the files as a comma delimited list to the star script.

elif sys.argv[1] == "PE" : # If paired end data
    print("Paired end mode")
    forward = copy.deepcopy(d)
    reverse = copy.deepcopy(d)
    
    for i in forward.keys():
        for x in range(len(forward[i])):
            forward[i][x] = "./trim_galore/" + forward[i][x] + "_pass_1_trimmed.fq.gz" # Add the file extension of the desired input files. Change if necessary.
    for i in reverse.keys():
        for x in range(len(reverse[i])):
            reverse[i][x] = "./trim_galore/" + reverse[i][x] + "_pass_2_trimmed.fq.gz" # Add the file extension of the desired input files. Change if necessary.

    for m,n in zip(forward.values(), reverse.values()): #Call the star aligner using the subprocees.call function.
        x = ",".join(m)
        z = ",".join(n)
        y = "sbatch star_PE.sh  " + x + " " + z
        #print y
        subprocess.call(y, shell=True) # gives the files as a comma delimited list to the star script.

```

Hopefully you will not have to fiddle to much with the above script. If it does not work at all contact me and I will fix it.
Run the above script as: `./start_input PE SRA_GSE#####.csv `. Note that the above and below scripts can be modfied to rename your alignment files based on the metadata so you know what sample is what.

The star scripts: 

I recommend to run STAR --help before all of this so that you understand what my parameters does.

For single end data.

##### star_SE.sh
```bash
#!/bin/bash -l

#SBATCH -J STAR
#SBATCH -n 16
#SBATCH -t 04:00:00

samples=${1}
genomedir=/proj/lassim/reference_genomes_OR/human/hg38_refseq/star_index

STAR --runThreadN 16 --outSAMtype BAM SortedByCoordinate --outSAMstrandField intronMotif --outFilterIntronMotifs RemoveNoncanonical --readFilesCommand zcat --genomeDir $genomedir --readFilesIn ${samples} --outTmpDir ${samples%%.*}.tmp. --outFileNamePrefix ${samples%%.*}.star.

```

for paired end data

##### star_PE.sh
```bash
#!/bin/bash -l

#SBATCH -J STAR
#SBATCH -n 16
#SBATCH -t 04:00:00

forward=${1}
reverse=${2}
genomedir=/proj/lassim/reference_genomes_OR/human/hg38_refseq/star_index

STAR --runThreadN 16 --outSAMtype BAM SortedByCoordinate --outSAMstrandField intronMotif --outFilterIntronMotifs RemoveNoncanonical --readFilesCommand zcat --genomeDir $genomedir --readFilesIn ${forward} ${reverse} --outTmpDir ${forward%%.*}.tmp. --outFileNamePrefix ${forward%%.*}.star.

```

Stringtie:
Now things get simple. First move all bam files from the trim_galore directory to a directory called stringtie. (mv ./trim_galore/*bam ./stringtie/)
Now run stringtie to assemble transcripts and quantify transcript expression. We are in this case using the shortend protocol exluding the stringtie merge step.
The input.
##### stringtie_input.sh
```bash
#!/bin/bash -l

for i in ./stringtie/*bam
do
	sbatch stringtie.sh ${i}
done
```
the stringtie script:

##### stringtie.sh
```
#!/bin/bash -l

#SBATCH -J Stringtie
#SBATCH -n 32
#SBATCH -N 1
#SBATCH -t 01:00:00

bam=$1

stringtie -e -p 32 -G /proj/lassim/reference_genomes_OR/human/hg38_refseq/GCA_000001405.15_GRCh38_full_analysis_set.refseq_annotation.gtf -o ${bam%%.bam}/expression.gtf $bam

```

Gtfs for hg19 ensemble and refseq are available in the /proj/lassim/reference_genomes_OR/human folder on sigma.

#### Summarize results from stringtie:
This is done with the script prepDE.py.
If evrything was run correctly running prepDE.py should be as simple as this: `prepDE.py -i ./stringtie/`
This will give you two count matrixes. One for genes and one for transcripts with columns named after the directory names of the samples.
Note that the gene and transcript names with the refseq database will be called just for example gene34 or gene56 for genes and rna4 or rna3002 for transcripts. Therefore to use the data I recommend to annotate these to their genebank name using the annotation in R.
Use the /proj/lassim/reference_genomes_OR/human/hg38_refseq/GCA_000001405.15_GRCh38_full_analysis_set.refseq_annotation.gff file for this. How to do this will be included in the R example script.
If you use the Ensemble database this problem will not occur. The problem stems from format diffrences between the refseq and ensemble annotation.

###Diffrential expression analysis.
Limma is not always applicable for public data but next we will move into R. An example script is provided.
Also Sigma has R studio and it runs pretty well if you use thinlinc and will install any package you want locally to your R folder. 
Alternatively you can download the processed data and use R studio on your local computer.





