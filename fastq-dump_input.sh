#!/bin/bash -l

#Find the files and save them to a variable.
samples=$(find ./sra/*sra -type f) 

mkdir fastq

for i in $samples
do
sbatch fastq-dump.sh ${i}
done
