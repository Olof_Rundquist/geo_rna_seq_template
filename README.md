A collection of scripts for downloading and processing RNA-seq data from GEO. 
See the Tutorial in the tutorial folder for instructions.
Note that many of the things presented here are situations unique to the NSC sigma and Tetralith clusters in Linköping, Sweden and might not be applicable to ex: Uppmax (Uppsala) or Galaxy.