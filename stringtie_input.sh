#!/bin/bash -l

for i in ./stringtie/*bam
do
	sbatch stringtie.sh ${i}
done
