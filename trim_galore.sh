#!/bin/bash -l

#SBATCH -t 01:00:00
#SBATCH -n 4
#SBATCH -J Trim_galore

files=${1} # ${1} = input 1. The list of files from the above script.

trim_galore -o ./trimmed_galore/ --nextera --fastqc --paired --retain_unpaired ${files} 
