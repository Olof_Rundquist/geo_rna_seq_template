#!/bin/bash -l

#Run this in the directory you want the data. Will create a directory with the sra data

GSE=$1

mkdir $GSE
mkdir ${GSE}/metadata
mkdir ${GSE}/sra

export SRAMETA=${GSE}/metadata
export SRARAW=${GSE}/sra

#This requires python 2.7
geofetch.py -i $GSE
